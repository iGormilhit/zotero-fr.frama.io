# Convertisseurs Zotero

*Consulter cette page dans la documentation officielle de Zotero : [Zotero Translators](https://www.zotero.org/support/translators) - dernière mise à jour de la traduction : 2022-10-12*

Zotero détecte automatiquement les articles de journaux, les notices des bibliothèques, les articles d'actualité et d'autres objets que vous pourriez souhaiter enregistrer dans votre bibliothèque Zotero. Zotero utilise des "convertisseurs" pour détecter et importer des données à partir de sites Web. Il existe actuellement plus de 600 convertisseurs différents, ce qui facilite l'importation de données à partir d'innombrables sites.

Vous pouvez voir le traducteur utilisé par Zotero sur une page donnée en passant la souris sur le bouton de sauvegarde de Zotero dans votre navigateur.

## Types de sites pris en charge

### Catalogues de bibliothèques

Zotero importe les notices de nombreux systèmes de catalogage de bibliothèques, ce qui permet une importation transparente à partir de centaines de bibliothèques universitaires et non universitaires. Les systèmes de catalogues de bibliothèques pris en charge sont les suivants : Aleph, Amicus, BiblioCommons, Dynix, Encore, Mango, InnoPAC, Primo, SirsiDynix, TLC/YouSeeMore, Voyager et WorldCat.

### Bases de données

Zotero importe des données, et dans de nombreux cas des PDF en texte intégral, des bases de données électroniques les plus populaires, notamment EBSCO, IEEEXplore, JSTOR, Google Scholar, ProQuest, PubMed et bien d'autres. Il fonctionne également avec la plupart des grands éditeurs de revues, notamment Cambridge University Press, Oxford University Press, Project MUSE, ScienceDirect (Elsevier), SpringerLink, Taylor and Francis, et bien d'autres encore.

### convertisseurs de sites individuels

Zotero dispose de convertisseurs spécialisés pour des centaines de sites Web, allant d'Amazon (dans plusieurs pays), The New York Times et The Economist, à Mainichi Daily News (Japon), Kommersant (Russie), Spiegel Online (Allemagne) et bien d'autres sites du monde entier.

### Importation de métadonnées

Zotero détecte et importe les métadonnées intégrées par un nombre croissant de sites Web et de bases de données dans des formats ouverts tels que COinS, Embedded RDF, les métabalises Google/HighWire et unAPI.

## Liste complète des convertisseurs

Vous pouvez consulter la liste des plus de 600 convertisseurs de Zotero, ainsi que leur code, sur le [dépôt GitHub Zotero Translator](https://github.com/zotero/translators/).

## Dépannage des convertisseurs

Si Zotero ne parvient pas à importer des données de haute qualité à partir d'un site qui, selon vous, devrait être pris en charge, vérifiez d'abord si vous rencontrez un problème général en essayant d'enregistrer un article de Wikipédia ou un livre d'Amazon. Si cela ne fonctionne pas non plus, consultez la section [Dépannage des problèmes liés aux convertisseurs](./troubleshooting_translator_issues.md). Si vous rencontrez des difficultés avec un site spécifique, signalez-le dans les forums Zotero en indiquant l'URL exacte que vous essayez d'utiliser.