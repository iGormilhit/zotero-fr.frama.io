---
hide:
  - navigation
  - toc
---

# Accueil

![logo de Zotero](./images/citeproc.svg){id=imghome}

Cette documentation a pour but de vous permettre de découvrir le logiciel Zotero et d'approfondir vos connaissances pour l'utiliser plus efficacement.

Il s'agit d'une traduction bénévole de la documentation officielle disponible en anglais sur [zotero.org](https://www.zotero.org/support/), avec l'autorisation de la [Corporation for Digital Scholarship](https://digitalscholar.org/) pour l'utilisation du nom et du logo Zotero. Cette traduction s'efforce de rester au plus proche des formulations originales : dans les pages traduites, "nous" renvoie ainsi à l'auteur original, c'est-à-dire l'équipe de développement de Zotero.



!!! Caution "Peinture fraîche"

    Cette documentation a fait l’objet d’un important travail de conversion et de reprise du contenu. Malgré notre relecture attentive il est possible qu’il reste des liens morts ou autres anomalies. N’hésitez pas à nous signaler celles-ci par [mail](mailto:contact@zotero-fr.org) ou en ouvrant un ticket dans notre [dépôt Framagit][repo].

!!! info "Documentation « Développeur »"

    La documentation à destination des développeur·ses et personnes ayant un usage avancé souhaitant par exemple créer ou modifier des styles n'a pas été traduite ici. Vous pouvez consulter la [documentation officielle en anglais de Zotero](https://www.zotero.org/support/dev/start).

# Contribuer à la documentation

Si vous souhaitez contribuer à la documentation, vous pouvez écrire à l'adresse [contact@zotero-fr.org](mailto:contact@zotero-fr.org) et/ou consulter les instructions du [dépôt Framagit][repo].

[repo]: https://framagit.org/zotero-fr/documentation-zotero-francophone
