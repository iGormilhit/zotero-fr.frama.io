# Pourquoi mes citations sont-elles soulignées par une ligne pointillée ?

*Consulter cette page dans la documentation officielle de Zotero : [Why are my citations underlined with a dashed line?](https://www.zotero.org/support/kb/citations_underlined) - dernière mise à jour de la traduction : 2022-10-18*

Lorsque vous insérez une citation dans votre document, Zotero doit souvent mettre à jour d'autres citations et la bibliographie de votre document pour refléter la prise en compte de cette nouvelle citation. Par exemple, si vous insérez une citation d'un auteur portant le même nom de famille que celui d'une autre citation dans le document, les consignes du style bibliographique peuvent exiger que chacun des noms inclut l'initiale du prénom pour aider les lecteurs à distinguer les auteurs.

![delayed-citations-2](../images/delayed-citations-2.png)

Lorsque les citations de votre document sont soulignées par une ligne pointillée, cela signifie que vous avez désactivé cette mise à jour automatique des citations. Le soulignement vous avertit que la citation n'est peut-être pas entièrement à jour ou correctement mise en forme. Dans les documents longs, la mise à jour des citations peut prendre un certain temps, et Zotero vous a probablement incité à désactiver la mise à jour automatique pour accélérer votre rédaction.

Pour mettre à jour toutes les citations dans votre document lorsque les citations automatiques sont désactivées, cliquez sur le bouton "Refresh" du module de traitement de texte.

Pour activer ou désactiver manuellement la mise à jour automatique des citations, ouvrez la fenêtre des préférences du document dans le module de traitement de texte et cochez ou décochez la case "Mettre à jour automatiquement les citations". Pour éviter de soumettre accidentellement un article avec des citations non mises en forme, nous vous recommandons de laisser les mises à jour automatiques activées, sauf si vous trouvez que les insertions prennent trop de temps pour un document en particulier.

![word_disable_automatic_updates](../images/word_disable_automatic_updates.png)
