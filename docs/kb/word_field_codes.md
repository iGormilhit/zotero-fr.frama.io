# Pourquoi est-ce que je vois du code commençant par ADDIN ZOTERO\_ITEM CSL\_CITATION dans mon texte au lieu de citations mises en forme ?

*Consulter cette page dans la documentation officielle de Zotero : [Why do I see code beginning with ADDIN ZOTERO_ITEM CSL_CITATION in my document instead of formatted citations?](https://www.zotero.org/support/kb/word_field_codes) - dernière mise à jour de la traduction : 2022-10-12*

Par défaut, Zotero stocke les données bibliographiques utilisées pour les citations et la bibliographie dans des [champs](https://support.office.com/fr-fr/article/ins%C3%A9rer-modifier-et-afficher-des-champs-dans-word-c429bbb0-8669-48a7-bd24-bab6ba6b06bb) (Word) ou des [Marques de référence](https://help.libreoffice.org/Writer/About_Fields/fr) (LibreOffice). Les données bibliographiques sont stockées de façon cachée derrière le texte mis en forme.

Si Word ou LibreOffice affiche les codes de champ au lieu du texte mis en forme, vous pouvez masquer les codes de champ en appuyant sur Alt/Option+F9 (ou Option+Fn+F9 sur Mac) dans Word ou Ctrl+F9 dans LibreOffice. Pour une citation unique dans Word, vous pouvez cliquer avec le bouton droit de la souris sur le code de champ et choisir "Basculer les codes de champ".

Si les codes de champ s'affichent de manière répétée, vérifiez les paramètres de votre traitement de texte pour l'affichage des champs.

-   Dans Word pour Windows, ouvrez Fichier -&gt; Options Word -&gt; Options avancées et décochez la case "Afficher les codes des champs au lieu de leurs valeurs" dans la rubrique "Affichage du contenu des documents".
-   Dans Word pour Mac, ouvrez Word -&gt; Préférences -&gt; Afficher et décochez la case "Codes des champs au lieu des valeurs" dans la zone "Afficher dans le document".
-   Dans [LibreOffice](https://help.libreoffice.org/Writer/Field_Names/fr), ouvrez Outils -&gt; Options -&gt; LibreOffice Writer -&gt; Affichage, et décochez la case "Noms des champs" dans la rubrique "Affichage".

Lorsque vous utilisez le suivi des modifications, Word ou LibreOffice affiche parfois à la fois la versions modifiée et la version originale des codes de champs qui ont été modifiés. Si cela se produit, cliquez deux fois sur "Accepter" pour accepter rapidement les citations modifiées et masquer à nouveau les codes de champs.


