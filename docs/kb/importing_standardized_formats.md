# Comment importer depuis BibTeX ou d'autres formats standards ?

*Consulter cette page dans la documentation officielle de Zotero : [How do I import BibTeX or other standardized formats?](https://www.zotero.org/support/kb/importing_standardized_formats) - dernière mise à jour de la traduction : 2022-09-14*

Zotero peut importer les données bibliographiques enregistrées dans une multiplicité de formats standardisés utilisées par des bases de données ou d'autres logiciels de gestion bibliographique. Les formats les plus populaires sont RIS, Bib(La)Tex, et MODS.

Si vos données se trouvent dans un fichier utilisant un de ces formats, par exemple une base BibTeX que vous avez compilée ou un fichier RIS exporté d'un autre logiciel de gestion bibliographique, vous pouvez les importer dans Zotero en sélectionnant Ficher → "Importer…" puis choisir "Un fichier".

Voir aussi [Importer des références depuis d'autres logiciels de gestion bibliographique](../moving_to_zotero.md).

## Formats pouvant être importés dans Zotero

-   Zotero RDF
-   CSL JSON
-   BibTeX
-   BibLaTeX
-   RIS
    -   Sa structure simple permet des modifications rapides entre l'exportation et l'importation des données
-   Bibliontology RDF
-   MODS (Metadata Object Description Schema)
-   Endnote XML
    -   Format à privilégier pour l'importation depuis Endnote
-   Citavi XML
    -   Format à privilégier pour l'importation depuis Citavi
-   MAB2
-   MARC
-   MARCXML
-   MEDLINE/nbib
-   OVID Tagged
-   PubMed XML
-   RefWorks Tagged
    -   Format à privilégier pour l'importation depuis RefWorks
-   Web of Science Tagged
-   Refer/BibIX
    -   N'utiliser ce format qu'en dernier recours, si aucun autre format n'est disponible
-   XML ContextObject
-   Unqualified Dublin Core RDF
