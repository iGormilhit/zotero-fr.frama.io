# Quelle est ma version de Zotero ?

*Consulter cette page dans la documentation officielle de Zotero : [What version of Zotero do I have?](https://www.zotero.org/support/kb/zotero_version) - dernière mise à jour de la traduction : 2022-09-14*

Vous pouvez vérifier votre version de Zotero en cliquant sur "À propos de Zotero" dans le menu Zotero (Mac) ou le menu "Aide" (Windows).

Assurez-vous que vous avez bien la dernière version indiquée dans le [changelog](https://www.zotero.org/support/changelog).

Vous pouvez mettre à jour vers la dernière version de Zotero en cliquant sur "Aide" -> "Vérifier les mises à jour…" dans Zotero ou - si ça ne fonctionne pas correctement - depuis la [page de téléchargement de zotero.org](https://www.zotero.org/download).