# Comment puis-je transférer ma bibliothèque Zotero vers un autre ordinateur ?

*Consulter cette page dans la documentation officielle de Zotero : [How can I move my Zotero library to a different computer?](https://www.zotero.org/support/kb/transferring_a_library) - dernière mise à jour de la traduction : 2022-10-06*

## Option A: Copier le répertoire de données Zotero

Si vous êtes à l'aise avec le déplacement de fichiers, la façon la plus fiable pour transférer entièrement votre bibliothèque Zotero vers un autre ordinateur est de copier le [répertoire des données Zotero](../zotero_data.md) depuis votre premier ordinateur vers votre nouvel ordinateur. Si vous remplacer votre ancien ordinateur, votre système d'exploitation peut vous proposer un moyen de déplacer automatiquement toutes vos données (par exemple l'Assistant migration sur un Mac).

Pour déplacer vos données manuellement, commencez par localiser vos données Zotero, en ouvrant les [préférences de Zotero](./preferences.md) et en cliquant sur "Ouvrir le répertoire de données" dans l'onglet "Avancées -&gt; Fichiers et dossiers". Reportez-vous à [la rubrique "Emplacements par défaut"](../zotero_data.md#emplacements_par_defaut.md) pour connaître les emplacements par défaut du répertoire de données.

Assurez-vous d'avoir fermé Zotero sur les deux ordinateurs avant de copier les fichiers Zotero. Si vous avez déjà utilisé Zotero sur le nouvel ordinateur, un répertoire de données existera déjà avec une base de données vide, et vous devriez supprimer le dossier entier avant de copier le nouveau dossier au même emplacement.

## Option B: Activer la synchronisation Zotero

Vous pouvez également [synchroniser](../sync.md) automatiquement votre bibliothèque Zotero entre plusieurs ordinateurs. Assurez-vous d'utiliser le même nom d'utilisateur sur tous les ordinateurs.

Vous aurez besoin d'assez d'espace de stockage en ligne pour contenir tous les fichiers de votre bibliothèque. Zotero vous avertira si vous avez atteint votre quota, ce qui nécessitera de supprimer certains fichiers, de payer un [abonnement pour le stockage](https://www.zotero.org/storage) ou de transférer votre bibliothèque en utilisant l'option A ci-dessus.

## N'utilisez pas l'Export / Import

Exporter et importer votre bibliothèque (par exemple via Zotero RDF) n'est pas une option recommandée. Aucun des formats d'export disponibles ne permet un transfert complet et sans perte des données de votre bibliothèque. De plus cela brisera les connexions existantes entre votre bibliothèque Zotero et vos fichiers de traitement de texte. Et si vous synchroniser plus tard vous obtiendrez des doublons des documents importés.