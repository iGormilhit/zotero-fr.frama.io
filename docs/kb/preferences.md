# Comment puis-je modifier les préférences de Zotero?

*Consulter cette page dans la documentation officielle de Zotero : [How do I change Zotero preferences?](https://www.zotero.org/support/kb/preferences) - dernière mise à jour de la traduction : 2022-09-14*

Les préférences de Zotero se trouvent dans le menu "Zotero" sur Mac et dans le menu "Édition" sur Windows et Linux.