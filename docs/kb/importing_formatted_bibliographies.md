# J'ai des bibliographies dans des documents Microsoft Word, des PDF, et d'autres fichiers textes. Puis-je les importer dans ma bibliothèque Zotero ?

*Consulter cette page dans la documentation officielle de Zotero : [have bibliographies in Microsoft Word documents, PDFs, and other text files. Can I import them into my Zotero library?](https://www.zotero.org/support/kb/importing_formatted_bibliographies) - dernière mise à jour de la traduction : 2022-09-20*


## Citations insérées à l'aide d'un logiciel de gestion bibliographique

Zotero peut lire les citations existantes créées par les modules de traitements de texte de Zotero et Mendeley, permettant de continuer à utiliser ces citations dans le même document, même si les documents n'existent pas dans votre bibliothèque Zotero. Cliquez simplement sur le bouton "Add/Edit Citation", cherchez pour une citation existante, et sélectionnez-la depuis la section "Cité" des résultats de la recherche. (Cette méthode s'applique seulement à la fenêtre de dialogue par défaut, pas à la fenêtre "Vue classique".)

Si un document contient des citations Zotero ou Mendeley qui ne sont pas dans votre bibliothèque et que vous avez besoin de modifier leurs métadonnées ou de les inclure dans d'autres documents, vous aurez besoin de les extraire vers votre bibliothèque. Pour les documents Word au format .docx, vous pouvez utiliser [Reference Extractor](https://rintze.zelle.me/ref-extractor/). Notez que pour continuer à utiliser le même document il vous faudra remplacer toutes les occurrences de la citation originale avec le nouveau document ajouté à votre bibliothèque, en vous assurant de bien sélectionner le document dans la section "Ma bibliothèque" dans les résultats de recherche de la fenêtre de dialogue, plutôt que la section "Cité".

Si les références sont encore dans un logiciel de gestion bibliographique, vous pouvez les exporter de ce logiciel vers un format de fichier permettant l'échange de métadonnées bibliographiques, comme RIS ou BibTeX, puis [importer le fichier ainsi généré](../adding_items_to_zotero.md#importer-depuis-dautres-outils) dans Zotero. Vous aurez besoin de remplacer toutes les citations existantes dans tout document pour lequel vous souhaitez pouvoir générer une bibliographie Zotero correcte.

## Citations insérées en utilisant la fonctionnalité de citation intégrée à Microsoft worked

Vous pouvez suivre les étapes suivantes pour formater la bibliographie en un fichier BibTeX, que Zotero peut importer.

1.  Téléchargez cette [feuille de style bibliographique Word](http://www.k-jahn.de/stuff/bibtex.xsl).
2.  Enregistrez la feuille de style dans le dossier des styles bibliographiques de Word :
    * *Word 2016 / 2019 / Office 365 pour Windows :* `C:\Utilisateurs\<NomUtilisateur>\AppData\Roaming\Microsoft\Bibliography\Style`
    * *Word 2010 pour Windows :* `C:\Program Files\Microsoft Office\<Office version>\Bibliography\Style` ou `C:\Program Files (x86)\Microsoft Office\<Office version>\Bibliography\Style`
    * *Mac :* Allez dans le dossier des Applications. Faites un clic-droit sur Microsoft Word et choisissez "Show Package Contents". Naviguer jusqu'à : `Content/Resources/Style`
3.  Dans Word, modifiez votre style bibliographique en sélectionnant "BibTeX export" et copiez la bibliographie dans le presse-papiers.
4.  Utilisez la fonctionnalité de Zotero [Importer depuis le presse-papiers](https://www.zotero.org/support/kb/import_from_clipboard).

Pour continuer à utiliser le même document, vous aurez besoin de remplacer toutes les citations originales afin que Zotero puisse générer une bibliographie correcte.

# Citations et bibliographies en texte simple

Si les références comportent un ISBN, un DOI, un PubMed ID ou un arXiv ID, vous pouvez utiliser la fonctionnalité de Zotero [Ajouter un document par son identifiant](../adding_items_to_zotero#ajouter_un_document_par_son_identifiant.md) pour importer rapidement ces documents dans votre bibliothèque Zotero.

Vous pouvez analyser les références bibliographiques en texte simple à l'aide de [AnyStyle](http://anystyle.io), un analyseur bibliographique en ligne écrit par un développeur de Zotero Exportez les citations analysées au format BibTeX ou CiteProc/JSON et importez-les dans Zotero. 

Sinon, votre meilleure option est de trouver les documents en ligne dans un dépôt ou une base bibliographique prise en charge par Zotero, ou, en dernier recours, de saisir manuellement les références.

Dans tous les cas, vous aurez besoin de remplacer toutes les citations de chaque document pour lequel vous souhaitez que Zotero puisse générer une bibliographie correcte.