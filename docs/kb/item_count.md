# Comment savoir combien de documents y a-t-il dans ma bibliothèque Zotero ?

*Consulter cette page dans la documentation officielle de Zotero : [How can I see how many items I have in my Zotero library?](https://www.zotero.org/support/kb/item_count) - dernière mise à jour de la traduction : 2022-09-20*

 Lorsqu'aucun document n'est sélectionné, le panneau de droite de Zotero affiche le nombre d'éléments dans la vue actuelle. Vous pouvez donc voir le nombre d'éléments dans une bibliothèque ou une collection donnée en effectuant simplement une sélection dans le panneau de gauche. Si des éléments sont actuellement sélectionnés, cliquez sur une autre collection et revenez dans la collection pour afficher le nombre d'éléments.

Pour déterminer le nombre total d'éléments, y compris les pièces jointes enfant et les notes, cliquez sur la liste des éléments, appuyez sur la touche "+" (plus) pour afficher tous les documents enfants, puis désélectionnez l'élément sélectionné (Cmd-clic (Mac) ou Ctrl-clic (Windows/Linux)) ou utilisez la fonction "Tout sélectionner" (Cmd-A (Mac) ou Ctrl-A (Windows/Linux)). Vous pouvez ensuite appuyer sur "-" (moins) pour replier tous les documents.

Pour déterminer combien de documents parents ont été trouvés lors d'une recherche qui affichent les documents enfants associés, cliquez sur la liste des éléments, appuyez sur la touche "-" (moins) pour réplier tous les documents, puis désélectionnez le document sélectionné avec Cmd-clic (Mac) ou Ctrl-clic (Windows/Linux). 