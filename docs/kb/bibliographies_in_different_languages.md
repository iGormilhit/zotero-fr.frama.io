# Puis-je utiliser Zotero dans une langue et créer des bibliographie dans une autre ?

*Consulter cette page dans la documentation officielle de Zotero : [Can I use Zotero in one language and create bibliographies in another?](https://www.zotero.org/support/kb/bibliographies_in_different_languages) - dernière mise à jour de la traduction : 2022-10-04*

Oui, voyez [Changer de langue](../supported_languages.md#changer-de-langue).

