# Pourquoi les changements que je fais ne sont-ils pas synchronisés entre plusieurs ordinateurs et/ou zotero.org ?

*Consulter cette page dans la documentation officielle de Zotero : [Why aren't changes I make syncing between multiple devices and/or zotero.org?](https://www.zotero.org/support/kb/changes_not_syncing) - dernière mise à jour de la traduction : 2022-10-06*

*Cette page couvre les problèmes concernant la synchronisationd des données. Pour les problèmes concernant la synchronisation des fichiers, consultez la page [Pourquoi ai-je le message "Le fichier joint n'a pu être trouvé. Il peut avoir été effacé ou déplacé hors de Zotero." quand j'essaie d'ouvrir un fichier depuis un ordinateur synchronisé, ou pourquoi est-ce que je ne peux pas télécharger un fichier dans l'app iOS ?](./files_not_syncing.md).*

## 1) Vous regardez au mauvais endroit

Dans les bibliothèques Zotero, tous les documents existants sont affichés soit à la racine de la bibliothèque, soit dans la corbeille. Assurez-vous que vous ne regardez pas dans une collection qui ne contient qu'une partie de vos documents. Si vous ne voyez pas le panneau gauche de Zotero, cliquez sur la barre située sur le bord gauche de la fenêtre pour l'afficher, ou vérifiez que "Panneau des collections" dans "Affichage -> Mise en page" est coché. Si vous ne voyez pas vos collections dans la liste, assurez-vous que le contenu de "Ma bibliothèque" est développé en cliquant sur la flèche ou le signe plus à côté de "Ma bibliothèque".

## 2) Le processus de synchronisation n'est pas terminé

Si vous êtes sûr de chercher au bon endroit, vérifiez si les données en question ont été [synchronisées](../sync.md) vers votre [bibliothèque en ligne sur zotero.org](https://www.zotero.org/mylibrary). Si vous ne voyez pas les données dans la bibliothèque en ligne, le problème se situe sur l'ordinateur d'où proviennent les données. Vérifiez l'icône verte de synchronisation dans Zotero sur l'appareil concerné. Si elle tourne toujours, le processus de synchronisation n'est pas encore terminé. Passez la souris sur l'icône de synchronisation pour voir l'état actuel.

## 3) Vous avez reçu une erreur de synchronisation

Si vous voyez une icône d'erreur rouge à gauche de l'icône de synchronisation (sur un ordinateur) ou une erreur en bas à gauche de l'écran (iOS), cela signifie qu'une erreur s'est produite pendant la synchronisation. Cliquez ou appuyez sur le message pour afficher l'erreur. Elle peut vous donner suffisamment d'informations pour que vous puissiez résoudre le problème vous-même, ou vous pouvez le signaler et publier [l'identifiant de rapport](../reporting_problems.md) sur les [forums de Zotero](https://forums.zotero.org) pour obtenir une aide supplémentaire.

## 4) La bibliothèque n'est pas paramétrée pour être synchronisée

Dans le [volet "Synchronisation"](../preferences_sync.md) des préférences de Zotero, cliquez sur "Choisir les bibliothèques..." et assurez-vous que la bibliothèque que vous essayez de synchroniser est cochée. Si ce n'est pas le cas, cliquez dans la colonne "Synchronisation" pour activer la synchronisation pour cette bibliothèque. Toutes les bibliothèques sont configurées pour être synchronisées par défaut.

## 5) Vérifiez les détails de votre compte Zotero

Vérifiez le [volet "Synchronisation"](../preferences_sync.md) des préférences de Zotero pour vous assurer que vous utilisez le même compte que celui que vous utilisez pour vous connecter sur zotero.org, et ce sur chacun de vos appareils. Si le problème concerne un groupe, assurez-vous que le compte est membre de ce groupe sur [zotero.org](https://www.zotero.org/groups).

## 6) Dépannage supplémentaire

Si vous rencontrez toujours des difficultés, publiez un message sur les forums Zotero en indiquant un [Debug ID](../debug_output.md#enregistrement-des-journaux-de-débogage) pour la première synchronisation lancée après une modification qui ne se synchronise pas (par exemple, la modification d'un document dans la bibliothèque en ligne qui n'apparaît pas dans Zotero, ou vice versa). Commencez l'enregistrement du journal de débogage avant d'effectuer la modification et continuez jusqu'à ce que la synchronisation soit terminée.