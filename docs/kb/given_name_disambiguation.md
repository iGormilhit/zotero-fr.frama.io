# Pourquoi certaines citations incluent-elles les prénoms ou leurs initiales et comment l'empêcher ?

*Consulter cette page dans la documentation officielle de Zotero : [Why do some citations include first names or initials, and how can I prevent this from happening?](https://www.zotero.org/support/kb/given_name_disambiguation) - dernière mise à jour de la traduction : 2022-10-05*


Parfois, certaines citations apparaissent sous la forme "(J. Doe, 2004)", alors même que votre style bibliographique n'affiche normalement que le nom de famille de l'auteur ("Doe 2004"). Dans ces cas, Zotero essaie de désambiguïser - distinguer - des auteurs différents, selon les règles du style bibliographique sélectionné. C'est généralement ce que vous voulez qu'il fasse, et si vous pensez le contraire, vous devez relire attentivement les consignes de votre style. Le style APA, par exemple, exige ce type de désambiguïsation.

La désambiguïsation peut toutefois également être effectuée si les nom et prénom d'un auteur sont enregistrés de manière incohérente dans votre bibliothèque Zotero. Par exemple, Zotero considère les noms suivants comme ceux d'individus distincts et les désambiguïsera selon les règles du style.
 
-   Jeff Smith
-   J. Smith
-   J. R. Smith

Vous pouvez résoudre ce problème en parcourant votre bibliothèque et en modifiant tous les noms qui renvoient à la même personne afin qu'ils aient exactement la même forme. Cela permettra à Zotero de désambiguïser les auteurs correctement dans ce style et dans les autres styles que vous utiliserez à l'avenir. 

Si vous ne souhaitez pas mettre à jour les noms dans votre bibliothèque, vous pouvez aussi simplement utiliser un style qui ne désambiguïse pas les noms. Consultez pour cela [le guide pas-à-pas pour désactiver la désambiguïsation dans un style CSL](https://www.zotero.org/support/dev/citation_styles/style_editing_step-by-step).


Zotero et CSL prennent en charge des [règles sophistiquées de désambiguïsation](http://citationstyles.org/downloads/specification.html#disambiguation). Si vous pensez qu'un style désambiguïse de manière incorrecte, veuillez publier un message sur le [forum de Zotero](https://forums.zotero.org/) et fournir de la documentation telle que les recommandations aux auteurs disponibles en ligne ou un article publié dans la revue en question. *Notez que ces changements ne règlent pas le problème des auteurs enregistrés de manière incohérente.*

## Autres causes

### Documents supprimés

Si vous vous êtes assuré que le nom de l'auteur est enregistré de manière cohérente dans Zotero, une ou plusieurs de vos citations peuvent pointer vers une référence que vous avez supprimée de Zotero. Dans ce cas, Zotero utilise les métadonnées incorporées dans le document. Pour vérifier si une citation est toujours liée à votre bibliothèque Zotero, cliquez sur la citation, cliquez sur "Add/Edit citation", cliquez sur la bulle bleue dans la boîte de dialogue d'ajout de citation (la barre rouge, non la vue classique), et recherchez un bouton "Ouvrir dans [Bibliothèque]" en bas de la fenêtre contextuelle. Si le bouton n'apparaît pas, la citation n'est plus liée à Zotero. Vous devrez supprimer la citation et la réinsérer, en veillant à la sélectionner dans la bibliothèque appropriée parmi les propositions de la boîte de dialogue d'ajout de citation.

### Doublons

Si vous avez une même référence en plusieurs exemplaires dans votre bibliothèque Zotero et que vous citez ces derniers à des endroits différents, Zotero traitera les auteurs comme distincts et les désambiguïsera dans le texte. Dans une même bibliothèque, vous pouvez résoudre ce problème en fusionnant les références dans votre bibliothèque Zotero, soit via la vue "Doublons", soit en les sélectionnant dans la liste principale de votre bibliothèque, en faisant un clic-droit, puis en choisissant "Fusionner les documents...", et enfin en cliquant sur le bouton "Refresh" de l'extension de traitement de texte. Si vous avez cité des références provenant de différentes bibliothèques, utilisez "Add/edit citation" comme décrit dans la section précédente pour identifier la référence associée à chaque citation et assurez-vous que chacune d'entre elles pointe vers la même référence dans une seule des bibliothèques.

### Désactivation de la mise à jour automatique des citations

Si vous voyez des citations avec des pointillés soulignés, vous avez désactivé [la mise à jour automatique des citations](https://zotero.hypotheses.org/1685#new_cite). Vous pouvez soit appuyer sur "Refresh" pour mettre à jour les citations manuellement, soit réactiver les mises à jour automatiques des citations dans le menu "Document preferences" du module de traitement de texte. 
