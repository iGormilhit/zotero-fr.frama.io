# Comment puis-je mettre en forme certains mots d'un titre: par ex. en italique, en exposant ou en indice ?

*Consulter cette page dans la documentation officielle de Zotero : [How do I use rich text formatting, like italics and sub/superscript, in titles?](https://www.zotero.org/support/kb/rich_text_bibliography) - dernière mise à jour de la traduction : 2022-10-04*

Pour utiliser ponctuellement la mise en forme enrichie, il faut ajouter manuellement les balises de types HTML suivantes dans les champs de votre bibliothèque Zotero.

-   &lt;i&gt; et &lt;/i&gt; pour *l'italique*
-   &lt;b&gt; et &lt;/b&gt; pour **le gras**
-   &lt;sub&gt; et &lt;/sub&gt; pour <sub>l'indice</sub>
-   &lt;sup&gt; et &lt;/sup&gt; pour <sup>l'exposant</sup>
-   &lt;span style="font-variant:small-caps;"&gt; et &lt;/span&gt; pour <span style="font-variant: small-caps;">les petites majuscules</span>
-   &lt;span class="nocase"&gt; et &lt;/span&gt; pour supprimer les règles de capitalisation (par exemple pour des expressions étrangères dans des titres anglais)

Dans le rendu bibliographique, Zotero remplace automatiquement ces balises par la mise en forme définie, par exemple "&lt;i&gt;Escherichia coli&lt;/i&gt; et les β-lactamases à spectre étendu" devient: "*Escherichia coli* et les β-lactamases à spectre étendu".  
Autre exemple: "La fluidité de l'eau dans nos cellules : H&lt;sub&gt;2&lt;/sub&gt;O, la molécule du vivant" devient: "La fluidité de l'eau dans nos cellules : H<sub>2</sub>O, la molécule du vivant".

Notez que si une mise en forme enrichie doit s'appliquer à l'intégralité du texte d'un champ (parce qu'une revue exige, par exemple, que les titres apparaissent en italiques), il faut alors modifier le style de citation CSL directement (consultez la [documentation sur les styles CSL](https://www.zotero.org/support/dev/citation_styles) et les [options CSL de mise en forme des champs](http://citationstyles.org/downloads/specification.html#formatting)). 

Une version ultérieure de Zotero permettra la modification visuelle de la mise en forme enrichie, sans l'ajout manuel de balises HTML.
