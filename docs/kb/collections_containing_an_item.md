# Comment voir dans quelles collections se trouve mon document ?

*Consulter cette page dans la documentation officielle de Zotero : [How can I see what collections my item is in?](https://www.zotero.org/support/kb/collections_containing_an_item) - dernière mise à jour de la traduction : 2022-09-20*

Pour visualiser toutes les collections auxquelles un document appartient, sélectionnez le document puis appuyez sur la touche “Alt” sur Linux, “Option” sur les Macs ou “Ctrl” sous Windows : les collections dans lesquelles est inclus le document seront alors surlignées.