# Quelle est la différence entre une capture et un lien vers la page d'un site web ?

*Consulter cette page dans la documentation officielle de Zotero : [What is the difference between Links and Snapshots?](https://www.zotero.org/support/kb/links_vs_snapshots) - dernière mise à jour de la traduction : 2022-09-14*

Lorsque vous joignez un lien à un document, Zotero enregistre seulement le titre de la page web, son adresse URL et sa date d'accès: il vous faut retourner sur le site pour visionner le contenu de la page.

Lorsque vous joignez une capture, Zotero enregistre une copie de la page comme elle existe à ce moment là et l'archive sur votre ordinateur.