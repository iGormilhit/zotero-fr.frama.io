# Abréviation des titres de revue

*Consulter cette page dans la documentation officielle de Zotero : [Journal Abbreviations](https://www.zotero.org/support/kb/journal_abbreviations) - dernière mise à jour de la traduction : 2022-10-04*

De nombreux styles bibliographiques exigent que les titres des revues soient abrégés, par exemple en utilisant une liste d'abréviations de revue.


À partir de la version 4.0 de Zotero, les titres de revue peuvent être automatiquement abrégés, selon le format Index Medicus/MEDLINE. Cette option est disponible dans [la fenêtre "Document Preferences"](../word_processor_plugin_usage.md#preferences-du-document) du module de traitement de texte de Zotero. Lorsqu'elle est désactivée, Zotero utilise l'abréviation saisie dans le champ « Abrév. de revue » de votre bibliothèque Zotero.

Il est prévu que Zotero puisse prendre en charge le choix entre plusieurs listes d'abréviations, car des styles différents peuvent utiliser des abréviations de revue différentes (par exemple *Proceedings of the National Academy of Sciences* peut être abrégé en *Proc. Natl. Acad. Sci. U. S. A.* ou en *PNAS*)

