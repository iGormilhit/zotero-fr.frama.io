# Où se trouve la barre d'outils Zotero dans Word pour Mac 2008 ?

*Consulter cette page dans la documentation officielle de Zotero : [Where is the Zotero toolbar in Word for Mac 2008?](https://www.zotero.org/support/kb/no_toolbar_in_word_2008_plugin) - dernière mise à jour de la traduction : 2022-09-28*

Le [module Zotero de traitement de texte](../word_processor_plugin_installation.md) pour Word 2008 pour Mac n'a pas de barre d'outils, mais à la place une entrée "Zotero" dans le menu AppleScript (l'icône d'un manuscrit à droite du menu Aide):

![/word2008toolbar_new.png](../images/word2008toolbar_new.png)

La plupart des commandes de menu sont aussi accessibles par des raccourcis clavier.

Word pour Mac 2008 ne prend pas en charge Visual Basic for Applications (VBA), ce qui rend impossible la création d'une barre d'outils. La prise en charge de VBA a été restaurée dans Word pour Mac 2011, et le module Zotero pour Word 2011 et 2016 inclut une barre d'outils (Word 2011) ou un onglet Zotero (Word 2016).


