# Ajouter des documents à Zotero

*Consulter cette page dans la documentation officielle de Zotero : [Adding items to Zotero](https://www.zotero.org/support/adding_items_to_zotero) - dernière mise à jour de la traduction : 2022-09-02*

Cette page décrit les différentes façons d'ajouter des documents (livres, articles de journaux, pages Web, etc.) comme documents Zotero. Pour en savoir plus sur l'ajout de fichiers (comme des PDF ou des images), veuillez consulter la page [Ajouter des fichiers à votre bibliothèque Zotero](./attaching_files.md).

## Via votre navigateur web

*Pour utiliser Zotero correctement, vous devez installer le connecteur Zotero pour Chrome, Firefox ou Safari, en plus de l'application de bureau Zotero. Voir la page de téléchargement [Download](/download).*

Le bouton d'enregistrement du connecteur Zotero est le moyen le plus pratique et le plus fiable d'ajouter des documents avec des métadonnées bibliographiques de haute qualité à votre bibliothèque Zotero. Lorsque vous naviguez sur le Web, Zotero trouvera automatiquement des informations bibliographiques sur les pages Web que vous visitez et vous permettra de les ajouter à Zotero d'un simple clic.

Par exemple, si vous lisez un article de revue en ligne, le bouton d'enregistrement de Zotero se transforme en l'icône d'un article de revue (entouré en rouge) :

![Capture d'écran montrant l'icône qui s'affiche pour indiquer la possibilité d'importer un article](./images/connector_firefox.png)

Sur une entrée de catalogue de bibliothèque pour un livre, le bouton d'enregistrement affiche une icône de livre :

![Icône indiquant la possibilité d'importer un livre](./images/connector_book.png)

Cliquez sur le bouton d'enregistrement pour créer dans Zotero un document avec les informations que Zotero a identifiées. Sur de nombreux sites, Zotero sauvegardera également tout PDF accessible depuis la page, ou un PDF en libre accès s'il peut en trouver un correspondant pour le document enregistré.

### Pages Web génériques

Certaines pages Web ne fournissent aucune information que Zotero puisse reconnaître. Sur ces pages, le bouton d'enregistrement affichera une icône de page Web grise. Si vous cliquez sur le bouton "Save to Zotero" sur ces pages, Zotero importera la page en tant que document "Page Web" avec un titre, une URL et une date d'accès. Voir [Enregistrer des pages Web](#enregistrer-des-pages-web) ci-dessous.

**Firefox:**  
![Icône générique, grisée, d'importation d'une page web](./images/connector_webpage_firefox.png)

**Safari:**  
![Icône générique, grisée, d'importation d'une page web](./images/connector_webpage_safari.png)

### PDF

Si vous affichez un fichier PDF dans votre navigateur, le bouton d'enregistrement affichera une icône PDF. Lorsque vous cliquerez sur ce bouton, Zotero importera le fichier PDF seul dans votre bibliothèque, puis il tentera automatiquement de [récupérer des informations](./retrieve_pdf_metadata.md) à son sujet. Bien que cela produise souvent de bons résultats, il est généralement préférable d'utiliser le bouton d'enregistrement décrit ci-dessus à partir de la page de résumé de la publication ou de l'entrée du catalogue.

![Icône d'importation d'un PDF](./images/connector_pdf.png)

Lorsque vous sauvegardez directement un PDF et que Zotero ne parvient pas à récupérer de métadonnées, il laissera le PDF comme élément indépendant. Pour ajouter des métadonnées, vous devez créer un document parent, soit en utilisant le bouton d'enregistrement, comme décrit ci-dessus, et en glissant le PDF sur ce nouveau document, soit en faisant un clic droit sur le PDF, puis en choisissant "Créer un document parent", et enfin en indiquant un identifiant tel qu'un DOI ou un ISBN. Si ces procédures ne fonctionnent pas, vous pouvez aussi cliquer sur "Saisie manuelle" après avoir sélectionné "Créer un document parent", et compléter manuellement les métadonnées pour le document.

### Résultats multiples

Sur certaines pages Web qui contiennent des informations concernant plusieurs documents (par exemple, une liste des résultats de la recherche Google Scholar), le bouton d'enregistrement affichera une icône de dossier. En cliquant sur cette icône de dossier, vous ouvrez une fenêtre dans laquelle vous pouvez sélectionner les documents que vous souhaitez enregistrer dans Zotero.

![Icône de dossier indiquant des références multiples sur une page](./images/connector_folder.png)

![Fenêtre affichant une liste de références, permettant le choix de celles à importer](./images/zotero-item-selector.png)

### Enregistrer dans une collection ou une bibliothèque spécifique

Après avoir cliqué sur le bouton d'enregistrement, une fenêtre contextuelle apparaîtra pour indiquer dans quelle collection Zotero le document est enregistré. Si vous souhaitez enregistrer le document dans une autre collection ou dans une autre bibliothèque, vous pouvez modifier la sélection, de même que saisir les marqueurs à associer au nouveau document.

### Qualité des données et choix d'un convertisseur

La qualité des données importées par Zotero est déterminée par les informations fournies sur la page Web. Certains sites Web fournissent des données de très haute qualité en utilisant une méthode standard pour fournir les données à Zotero (via des métadonnées intégrées). D'autres sites Web ne fournissent que des métadonnées limitées (par exemple, seulement le titre d'un article de blog) ou aucune métadonnée du tout. Pour de nombreux sites, Zotero dispose de "convertisseurs" spécifiques à ces sites Web, afin d'obtenir des métadonnées de la meilleure qualité. Zotero reconnaît presque tous les catalogues de bibliothèques, la plupart des sites de presse, les bases de données de recherche et les éditeurs scientifiques. Par défaut, les mises à jour des convertisseurs sont installées automatiquement, indépendamment des mises à jour de Zotero. La qualité des métadonnées pour un même document peut varier d'un site à l'autre. Par exemple, importer un document à partir du site Web de l'éditeur produira généralement de bien meilleures données que l'importer à partir de Google Scholar.

Zotero choisira généralement automatiquement le meilleur convertisseur disponible pour chaque site. Vous pouvez choisir un autre convertisseur en cliquant avec le bouton droit de la souris sur le bouton d'enregistrement Zotero (ou le fond de la page dans Safari) et en choisissant une des options proposées. Si l'import depuis un site Web ne s'effectue pas correctement, veuillez le signaler sur le [forum Zotero](https://forums.zotero.org) et fournir l'URL de la page Web.

## Ajouter un document par son identifiant

![](./images/add-item-by-identifier-popup.png)

Vous pouvez rapidement ajouter des documents à votre bibliothèque si vous connaissez déjà leur ISBN ou leur identifiant DOI, PubMed ou arXiv. Pour ajouter un document via l'un de ces identifiants, cliquez sur le bouton "Ajouter un document par son identifiant" (![](./images/toolbar-lookup.png)) en haut de la colonne centrale du volet Zotero, tapez ou collez l'identifiant, puis appuyez sur Entrée/Retour. Pour entrer plusieurs identifiants en même temps, tapez le premier identifiant, puis appuyez sur Maj+Entrée/Retour, puis tapez les autres identifiants (un sur chaque ligne). Une fois que vous avez tapé tous les identifiants, appuyez sur Maj+Entrée/Retour pour importer tous les éléments en même temps. Vous pouvez également coller une liste de plusieurs identifiants (chacun sur une ligne séparée), puis appuyer sur Maj+Entrée/Retour pour terminer.

Zotero utilise les bases de données suivantes pour rechercher les métadonnées des documents : Library of Congress et [WorldCat](http://www.worldcat.org/) pour les ISBN, [CrossRef](http://www.crossref.org/) et d’autres registres pour les DOI, [NCBI PubMed](http://www.ncbi.nlm.nih.gov/pubmed/) pour les PMID et [arXiv](https://arxiv.org) pour les identifiants arXiv.

## Ajouter des PDF et d'autres fichiers

Comme noté ci-dessus, lorsque c'est possible, nous recommandons d'enregistrer des documents en [utilisant le bouton d'enregistrement du connecteur Zotero](#via-votre-navigateur-web) dans votre navigateur web, à partir de la page principale (p. ex. la page d'abstract d'un article de revue), plutôt que d'ajouter directement des PDF. En général, le bouton d'enregistrement téléchargera automatiquement des métadonnées de bonne qualité, ainsi que le PDF si vous y avez accès.

S'il n'y a pas de page de présentation du document, vous pouvez utiliser le bouton d'enregistrement lorsque vous regardez le PDF dans votre navigateur web.

Lorsque toutefois vous avez un PDF ou un autre fichier sur votre ordinateur, par exemple parce que vous l'avez reçu par courriel, vous pouvez simplement le glisser dans Zotero, soit vers un document existant pour joindre le fichier, ou entre des documents pour stocker le fichier comme élément indépendant. Vous pouvez également ajouter des fichiers comme pièces jointes à des documents Zotero existants en cliquant avec le bouton droit de la souris sur le document Zotero et en choisissant "Ajouter une pièce jointe", ou en cliquant sur l'icône du trombone dans la barre d'outils Zotero. Vous pouvez également ajouter des fichiers comme éléments indépendants en utilisant les options "Stocker une copie du fichier..." ou "Lien vers un fichier..." dans le menu "Nouveau document" (![Icône verte avec le signe +](./images/add.png)).

Comme les fichiers stockés comme éléments indépendants ne peuvent pas avoir de métadonnées bibliographiques ni de notes filles, il sera préférable dans la plupart des cas de créer un document parent. Lorsque vous ajouter un PDF comme élément indépendant, Zotero tentera automatiquement de [récupérer des métadonnées bibliographiques pour ces PDF](./retrieve_pdf_metadata.md) et de créer un document parent, bien que dans certains cas cela puisse produire des métadonnées de qualité inférieure à l'importation en utilisant le bouton d'enregistrement dans Zotero de votre navigateur. Si Zotero n'est pas en mesure de récupérer des métadonnées de haute qualité pour un PDF, il laissera ce dernier comme élément indépendant. Dans ce cas, vous pouvez enregistrer le document d'une autre manière : en utilisant le bouton "Save to Zotero" de votre navigateur puis en glissant le PDF sur ce nouveau document (si un PDF n'était pas déjà joint automatiquement), ou en faisant un clic droit sur le fichier indépendant, en choisissant "Créer un document parent", et enfin en indiquant un identifiant tel qu'un DOI ou un ISBN. Si ces procédures ne fonctionnent pas, vous pouvez aussi cliquer sur "Saisie manuelle" après avoir sélectionné "Créer un document parent", et compléter manuellement les métadonnées pour le document.

## Enregistrer des pages Web

Avec Zotero, vous pouvez créer un document à partir de n'importe quelle page Web en cliquant sur le bouton d'enregistrement dans la barre d'outils du navigateur. Si la page n'est pas reconnue par un [convertisseur](#web_translators), vous verrez l'icône de page Web grise. Si la page a bien un convertisseur reconnu, vous pouvez toutefois forcer Zotero à enregistrer un document de type page Web (au lieu du type reconnu par le convertisseur) en cliquant avec le bouton droit de la souris (cliquer et maintenir enfoncé dans Safari) sur le bouton d'enregistrement Zotero et en choisissant "Save to Zotero (Web Page with/without Snapshot)".

**Firefox:**  
![Icône générique, grisée, d'importation d'une page web](./images/connector_webpage_firefox.png)

**Safari:**  
![Icône générique, grisée, d'importation d'une page web](./images/connector_webpage_safari.png)

Si l'option "Faire une capture automatique de la page lors de la création de documents à partir de pages Web" est activée dans les [les préférences générales](./general.md) de Zotero, une copie (ou capture) de la page Web sera enregistrée sur votre ordinateur et ajoutée comme élément enfant. Vous pouvez également enregistrer une capture avec ce paramètre désactivé en cliquant avec le bouton droit de la souris (cliquer et maintenir enfoncé dans Safari) sur le bouton d'enregistrement Zotero et en choisissant l'option pertinente. Pour afficher la copie enregistrée, double-cliquez sur l'élément ou la capture dans Zotero.

![](./images/web_snapshot_356x36.png)

Double-cliquer sur un document de type page Web sans capture dans votre bibliothèque vous amènera à la page Web originale. Double-cliquer sur un document de type page Web avec capture affichera à la place la capture. Vous pouvez également consulter la page web originale en cliquant sur l'intitulé "URL :" à gauche du champ `URL` dans le panneau de droite de Zotero.

## Importer depuis d'autres outils

Voir [Importer depuis d'autres logiciels de gestion bibliographique](./moving_to_zotero.md).

## Imports massifs depuis des bases de données

Si vous importez un grand nombre de documents à partir de bases de données bibliographiques (par exemple, si vous menez une revue systématique), des bases de données comme Google Scholar, ProQuest, Web of Science et d'autres peuvent vous bloquer si vous utilisez le bouton d'enregistrement Zotero trop souvent ou pour un trop grand nombre de documents à la fois. Dans ce cas, il est préférable d'exporter les documents par lot dans l'un des [formats standardisés](./kb/importing_standardized_formats.md) (BibTeX et RIS sont par exemple des choix courants), puis d'importer ce fichier dans Zotero. Web of Science et ProQuest offrent la possibilité de sélectionner plusieurs éléments dans une liste de résultats de recherche et de les exporter par lot dans divers formats. Dans Google Scholar, vous devez d'abord enregistrer les documents dans votre bibliothèque Google Scholar (en utilisant l'icône ☆ dans les résultats de recherche), puis les sélectionner et les exporter à partir de la page "Ma bibliothèque" de Google Scholar.

## Ajouter manuellement des documents

Zotero est conçu pour vous éviter la saisie manuelle aussi souvent que possible. Généralement, vous devriez vérifier si vous pouvez ajouter des documents à Zotero [en passant par votre navigateur web](#via-votre-navigateur-web), plutôt qu'en les saisissant manuellement. Quand vous enregistrez depuis le web, Zotero va automatiquement importer des métadonnées de bonne qualité et importer des PDF lorsqu'ils sont disponibles, vous faisant ainsi gagner du temps tout en diminuant les erreurs. Même lorsque des corrections manuelles sont nécessaires, il est préférable de partir des données importées par Zotero plutôt que de créer un document en partant complètement de zéro.

Toutefois, il arrive qu'un document doive être saisi entièrement manuellement, par exemple parce qu'il ne figure dans aucun catalogue en ligne. Dans ce cas, cliquez sur le bouton vert "Nouveau document" (![Icône verte avec le signe +](./images/add.png)) en haut du volet central, et sélectionnez le type de document souhaité dans le menu déroulant. Le niveau supérieur du menu affiche les types de document récemment créés ; la liste complète des types de document, à l'exception de la page Web, se trouve sous la rubrique "Plus". Un document vide du type de document sélectionné apparaît alors dans la colonne centrale. Vous pouvez ensuite saisir manuellement les informations bibliographiques du document via le volet de droite.

**Remarque:** Puisqu'il est presque toujours préférable d'ajouter un document grâce au bouton "Save to Zotero" de votre navigateur, "Page Web" n'est pas inclus comme type de document dans le menu "Nouveau document". Toutefois, si vous voulez vraiment créer un document de page Web à la main, créez un document vide d'un autre type et changez le type de document à Page Web dans le volet de droite.

## Editer des documents

Lorsque vous avez sélectionné un document dans le volet central, vous pouvez afficher et modifier ses informations bibliographiques via l'onglet Info du volet de droite. La plupart des champs peuvent être cliqués et modifiés. Les modifications sont sauvegardées automatiquement au fur et à mesure qu'elles sont effectuées. Certains champs ont des caractéristiques particulières, qui sont décrites ci-dessous.

#### Noms

Chaque document peut avoir zéro ou plusieurs créateurs, de types différents, tels que des auteurs, des éditeurs, etc. Pour modifier le type de créateur, cliquez sur l'intitulé du champ "créateur" (par exemple, `Auteur:`). Un créateur peut être supprimé en cliquant sur le bouton "moins" à la fin du champ "créateur". Des champs "créateur" supplémentaires peuvent être ajoutés en cliquant sur le bouton "plus" à la fin du dernier champ "créateur". Les créateurs peuvent être réorganisés en cliquant sur l'intitulé d'un champ "créateur" et en sélectionnant "Descendre" ou "Remonter".

Chaque champ de nom peut être basculé entre un mode à champ unique et un mode à deux champs, en cliquant sur le bouton "Afficher un champ unique" / "Afficher deux champs" à la fin du champ "créateur". Le mode à champ unique doit être utilisé pour les institutions (par exemple, lorsque l'auteur est "Société A"), tandis que le mode à deux champs (nom de famille, prénom) doit être utilisé pour les noms de personne. Si une personne n'a qu'un seul nom (par exemple "Socrate"), inscrivez-le comme nom de famille dans le mode à deux champs. Dans le mode à deux champs, vous pouvez inverser les deux informations en cliquant avec le bouton droit de la souris sur le nom et en choisissant "Inverser prénom/nom".

Pour saisir rapidement d'autres créateurs, tapez Maj-Entrée/Retour pour passer immédiatement à un nouveau champ créateur.

#### Titres abrégés de revue

Les articles de revues sont souvent cités avec le titre abrégé de la revue. Zotero stocke le titre de la revue et l'abréviation du titre de la revue dans des champs séparés (respectivement "Publication" et "Abrév. de revue"). Bien que certains styles de citation exigent des abréviations différentes, la principale variation réside dans le fait que l'abréviation contienne ou non des points (par exemple, "PLoS Biol" ou "PLoS Biol."). Comme il est plus fiable de supprimer des points que d'en ajouter, nous vous recommandons de stocker les abréviations de titre dans votre bibliothèque Zotero avec les points. Zotero peut alors enlever de façon sûre les points dans les bibliographies générées lorsque le style de citation choisi l'exige.

#### Titres

Nous recommandons de toujours conserver les titres en casse dite de phrase (sentence case), c'est-à-dire avec seulement une lettre capitale en début de phrase (par opposition à la pratique anglophone de mettre des majuscules aux initiales des tous les mots). Voir [Casse de phrase](./kb/sentence_casing.md) pour plus d'informations.

#### Liens

En cliquant sur l'intitulé des champs URL ("URL:") et DOI ("DOI:"), vous affichez la page Web correspondante dans votre navigateur.

#### Extra

Le champ Extra peut être utilisé pour stocker des informations personnalisées ou des données qui n'ont pas de champ dédié dans Zotero. Si vous devez citer un document en utilisant un champ non fourni par Zotero, vous pouvez également stocker des données de ce type dans Extra. Voir [Citing Fields from Extra](./kb/item_types_and_fields.md#citing_fields_from_extra) pour plus de détails sur la manière de citer ces champs. Par exemple, pour ajouter un DOI à un document de type chapitre de livre , ajoutez-le au début du champ Extra : `DOI : 10.1234/567890`

## Verifier et modifier vos enregistrements

**Lorsque vous utilisez Zotero (ou toute autre logiciel de gestion bibliographique) pour citer, vous devriez toujours vérifier l'exactitude des informations après avoir enregistré des documents dans votre bibliothèque.**

Zotero importera avec précision les métadonnées fournies par la plupart des bases de données bibliographiques, catalogues de bibliothèques, sites d'éditeurs et pages Web. Il apportera même des ajustements aux métadonnées pour compenser les bizarreries connues (noms des auteurs en majuscules, etc.) dans ce que le fournisseur fournit. Cela dit, les métadonnées que Zotero reçoit sont parfois incomplètes ou incorrectes. Par exemple, un grand site de recherche universitaire fournit souvent le mauvais titre de revue avec des métadonnées par ailleurs correctes. Les métadonnées d'un autre site de recherche académique peuvent omettre certains des noms des auteurs ou les présenter dans le mauvais ordre. Même les grands éditeurs fournissent parfois les prénoms et les noms de famille des auteurs dans le mauvais ordre, et cela de façon aléatoire pour un même volume et un même numéro de la revue ou même pour un même article. Certaines métadonnées des éditeurs peuvent omettre des éléments importants (noms d'auteurs lorsqu'ils sont nombreux, ISSN des revues, DOI, etc.)

Certaines métadonnées sont fournies avec seulement le nom de famille des auteurs et une ou deux initiales, alors que le nom complet des auteurs figure sur le texte intégral de l'article. Pour que les noms d'auteur soient correctement désambiguïsés dans les styles auteur-date, le nom des auteurs doit être saisi de manière cohérente et identique pour tous les documents auxquels ils ont contribué.

Les éditeurs ont des conventions différentes pour la casse des titres. Aucun logiciel ne peut convertir de façon exacte et fiable le mode titre en mode phrase. Vous devriez toujours [conserver les titres en casse de phrase](./kb/sentence_casing.md) et laisser Zotero les convertir si le style l'exige.

Les utilisateurs de Zotero doivent être conscients de ces problèmes et vérifier que les enregistrements de leur bibliothèque sont exacts et dans le bon format, afin que Zotero puisse produire des citations bien formées dans le texte et la bibliographie de leur manuscrit. L'un des principaux avantages d'utiliser un logiciel de gestion bibliographique est que, une fois que vous en avez corrigé les données une fois, vos références au document seront toujours correct à l'avenir, peu importe le nombre de fois que vous le citez.
