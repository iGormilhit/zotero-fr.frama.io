# Importer depuis d'autres logiciels de gestion bibliographique

*Consulter cette page dans la documentation officielle de Zotero : [Importing from Other Reference Managers](https://www.zotero.org/support/moving_to_zotero) - dernière mise à jour de la traduction : 2022-09-02*

Il est possible d'importer dans Zotero les données provenant d'autres logiciels. Voici les procédures pour les logiciels les plus populaires :

-   [Mendeley](https://www.zotero.org/support/kb/mendeley_import)
-   [Endnote](./kb/importing_records_from_endnote.md)
-   [Citavi](https://www.zotero.org/support/kb/import_from_citavi)
-   [Fichiers textes (par ex. Word)](./kb/importing_formatted_bibliographies.md)
-   [Formats standardisés (tels que Bib(La)TeX ou JabRef)](./kb/importing_standardized_formats.md)

Il est aussi possible d'importer les données provenant d'autres outils, tels que Reference Manager, RefWorks, Papers, Google Scholar Library, ReadCube, etc. Il faut d'abord exporter les données dans un [format standard](./kb/importing_standardized_formats.md) tel que RIS, BibTeX, CSL JSON, puis importer ce fichier dans Zotero en sélectionnant Ficher → "Importer…" puis choisir "Un fichier".

Si vous souhaitez récupérer les données d'un installation de Zotero sur un autre ordinateur, consultez la page [Comment puis-je transférer ma bibliothèque Zotero vers un autre ordinateur ?](./kb/transferring_a_library.md).
