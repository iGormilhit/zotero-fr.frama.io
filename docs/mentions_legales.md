---
hide:
  - navigation
---

# Mentions légales et vie privée

## Mentions légales

### Responsable de publication

* Collectif Traduction-Zotero-fr  
* [contact@zotero-fr.org](mailto:contact@zotero-fr.org)

### Hébergeur

Ce site est proposé grâce au service [Framagit](https://framagit.org) de l'association [Framasoft](https://framasoft.org), hébergé chez la société [Hetzner Online GmbH](https://www.hetzner.com/) :

**[Framasoft](https://framasoft.org)**  

* Association loi 1901 déclarée en sous-préfecture d’Arles le 2 décembre 2003 sous le n° 0132007842  
* N° Siret : 500 715 776 00026  
* Siège social : Association Framasoft  
* Pierre-Yves Gosset  
c/o Locaux Motiv  
10 bis, rue Jangot  
69007 LYON  
FRANCE

Contacter Framasoft : [contact.framasoft.org](https://contact.framasoft.org)

**[Hetzner Online GmbH](https://www.hetzner.com/)**

* Industriestr. 25  
91710 Gunzenhausen  
Germany

* Tél.: +49 (0)9831 505-0

### Licence des contenus

Tous les contenus sont sous licence [Creative Commons BY SA](https://creativecommons.org/licenses/by-sa/4.0/deed.fr) (Paternité et réutilisation sous les mêmes conditions)

### Propriété intellectuelle

Zotero est un projet de la Corporation for Digital Scholarship](https://www.zotero.org/support/terms/trademark). Il a été créé au [Roy Rosenzweig Center for History and New Media](https://rrchnm.org/) de l'Université George Mason. 

[Zotero](https://zotero.org) est une marque déposée de la [Corporation for Digital Scholarship](https://www.zotero.org/support/terms/trademark). 

## Vie privée

### Données personnelles

Aucune donnée personnelle n'est collectée en dehors des statistiques web.

### Statistiques web 

Nous analysons le trafic de ce site avec Matomo. Matomo génère un cookie avec un identifiant unique, dont la durée de conservation est limitée à 13 mois. Les données recueillies (adresse IP, User-Agent…) sont anonymisées et conservées pour une durée de 6 mois. Elles ne sont pas cédées à des tiers ni utilisées à d’autres fins. Nous utilisons le [service Matomo de l'association Framasoft](https://stats.framasoft.org).

Nous n'avons pas besoin de recueillir votre consentement avant de déposer le cookie Matomo car le Matomo de Framasoft est configuré conformément aux [recommandations de la CNIL](https://www.cnil.fr/fr/cookies-et-autres-traceurs/regles/cookies-solutions-pour-les-outils-de-mesure-daudience).  
Vous pouvez cependant opt-out afin qu'aucune donnée ne soit collectée :

<iframe title="Matomo" src="https://stats.framasoft.org/?module=CoreAdminHome&amp;action=optOut&amp;language=fr" style="border: 0px; max-width: 600px; width: 85%;"></iframe>

Vous pouvez consulter la fiche du service Matomo du registre des traitements de Framasoft pour prendre connaissance des données collectées par Matomo et stockées dans le cookie.