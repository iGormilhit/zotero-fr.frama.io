# Préférences : Citer

*Consulter cette page dans la documentation officielle de Zotero : [Preferences : Cite](https://www.zotero.org/support/preferences/cite) - dernière mise à jour de la traduction : 2022-09-01*


Le volet “Citer” comporte deux onglets : "Styles" et "Traitements de texte".

## Les styles

![preferences_cite_styles_fr.png](./images/preferences_cite_styles_fr.png)

### Le gestionnaire de styles

Le gestionnaire de styles affiche les styles actuellement installés et la date à laquelle ils ont été mis à jour. Vous pouvez télécharger des styles supplémentaires directement depuis [l'entrepôt des styles Zotero](http://zotero.org/styles) en cliquant sur le lien “Obtenir d'autres styles…”. Vous pouvez également installer un fichier de style CSL ([Citation Style Language](http://citationstyles.org/)) local en cliquant sur le bouton '+', puis en trouvant le fichier de style sur votre ordinateur. Pour retirer un style, sélectionnez le style et cliquez sur le bouton "-".

Si vous avez des incertitudes concernant le style dont vous avez besoin, vous pouvez effectuer une [recherche par exemple](http://editor.citationstyles.org/searchByExample/) pour trouver un style adéquat. Notez que cet outil vous demande de mettre en forme les références déjà affichées sur la page, et non n'importe quel exemple de référence.

### Options de citation

-   **Inclure les adresses URL des articles de journaux dans les références :** Quand cette option est désactivée, Zotero inclut les adresses URL lorsque vous citez des articles de revue, de magazine et de journal uniquement si l'article ne comporte pas de numéros de page.

### Outils

-   **Editeur de style :** Ouvre la [fenêtre de l'éditeur de style](https://www.zotero.org/support/dev/citation_styles/reference_test_pane) de Zotero, pour modifier et tester des styles CSL. Vous pouvez également modifier des styles en utilisant un éditeur de texte installé sur votre ordinateur (i. e., Notepad, Text Edit) ou [l'éditeur visuel CSL](http://editor.citationstyles.org/visualEditor/).
-   **Aperçu des styles :** Ouvre la [fenêtre de l'aperçu des styles](https://www.zotero.org/support/dev/citation_styles/preview_pane), pour comparer tout ou partie des styles que vous avez installés, en générant des appels de citation et une bibliographie à partir de documents sélectionnés dans votre bibliothèque.

## Traitements de texte

![preferences_cite_processors_fr.png](./images/preferences_cite_processors_fr.png)

Les [modules de traitement de texte de Zotero](./word_processor_integration.md) permettent une intégration dans Microsoft Word ou LibreOffice. Zotero installera automatiquement les modules dans Word et LibreOffice lorsque vous installerez Zotero. Vous pouvez réinstaller ces modules depuis cet onglet. Réinstaller le module LibreOffice peut être utile si la mise à jour de LibreOffice modifie le chemin d'accès aux fichiers du programme LibreOffice.

Si l'un des boutons d'installation est désactivé, vérifiez que le module de traitement de texte correspondant est installé et activé dans la fenêtre des extensions de Zotero (cliquez sur "Outils -&gt; Extensions").

-   **Utiliser la fenêtre classique d'ajout de citation:** Par défaut, les modules de traitement de texte de Zotero vont utiliser une [interface de citation rapide](./word_processor_plugin_usage.md#citer), qui vous permet de rechercher intuitivement des documents dans toutes vos bibliothèques, d'ajouter plusieurs documents à la même citation, et d'ajouter facilement des numéros de page, des préfixes et des suffixes. Pour chercher un document en parcourant vos bibliothèques et collections, cliquez sur le "Z" à gauche de la fenêtre afin de basculer vers la ["fenêtre classique" d'ajout de citation](./word_processor_plugin_usage_classic.md) . Cochez cette option pour basculer de l'interface par défaut vers la fenêtre classique. Notez que certaines fonctions ne sont pas prises en charge dans la vue classique.


